package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Department;
import beans.Shop;
import beans.User;
import service.DepartmentService;
import service.Login_id_Service;
import service.ShopService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Shop> shops = new ShopService().getshop();
		List<Department> departments = new DepartmentService().getDepartment();


		request.setAttribute("shops", shops);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Shop> shops = new ShopService().getshop();
		List<Department> departments = new DepartmentService().getDepartment();


		request.setAttribute("shops", shops);
		request.setAttribute("departments", departments);


		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();

			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setShop_id(Integer.parseInt(request.getParameter("shop")));
			user.setDepartment_id(Integer.parseInt(request.getParameter("department")));

			new UserService().register(user);


			response.sendRedirect("management");
		} else {
			User user = new User();

			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setShop_id(Integer.parseInt(request.getParameter("shop")));
			user.setDepartment_id(Integer.parseInt(request.getParameter("department")));

			request.setAttribute("user",user);

			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}
//	private User getUser(HttpServletRequest request)
//            throws IOException, ServletException {
//
//        User user = new User();
//        user.setId(Integer.parseInt(request.getParameter("id")));
//        user.setLogin_id(request.getParameter("login_id"));
//        user.setName(request.getParameter("name"));
//        user.setPassword(request.getParameter("password"));
//        user.setShop_id(Integer.parseInt(request.getParameter("shop")));
//        user.setDepartment_id(Integer.parseInt(request.getParameter("department")));
//
//
//
//        return user;
//    }

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		int shop = Integer.parseInt(request.getParameter("shop"));
		int department = Integer.parseInt(request.getParameter("department"));

		User editUser = new Login_id_Service().duplicate(login_id);

		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("アカウント名を入力してください");
		}else if (!(login_id.matches ("^[0-9a-zA-Z]+$")  )) {
			messages.add("半角英数で入力してください");
		}
		if (20 < login_id.length()){
			messages.add("アカウント名:20字以下に設定してください");
		} if (6 > login_id.length()){
			messages.add("アカウント名:6字以上に設定してください");
		}if(editUser != null ) {
			messages.add("アカウント名:重複しています");
		}

		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}else if (!(password.matches ("^[¥x20-¥x7F]+$")  )) {
			messages.add("パスワード:半角英数記号で入力してください");
		}if (20 < password.length()){
			messages.add("パスワード:20字以下に設定してください");
		}if (6 > password.length()){
			messages.add("パスワード:6字以上に設定してください");
		}if(!(password.equals(password2))) {
			messages.add("パスワード:一致しません");
		}

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名称を入力してください");
		}if (10 < name.length()){
			messages.add("名称:10字以下に設定してください");
		}

		if(shop == 1 && (department == 3 || department == 4)) {
			messages.add("本店:支店と役職が一致しません");
		}
		else if(shop == 2 && (department == 1 || department == 2)) {
			messages.add("支店A:支店と役職が一致しません");
		}
		else if(shop == 3 && (department == 1 || department == 2)) {
			messages.add("支店B:支店と役職が一致しません");
		}
		else if(shop == 4 && (department == 1 || department == 2)) {
			messages.add("支店C:支店と役職が一致しません");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}