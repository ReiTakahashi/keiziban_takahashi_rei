package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


		String start = request.getParameter("start");//jspから貰う
		String finish = request.getParameter("finish");
		String categoryrefinement = request.getParameter("category");

        List<UserMessage> messages = new MessageService().getMessage(start, finish,categoryrefinement);
        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("messages", messages);//jspに対して渡す
        request.setAttribute("start", start);
        request.setAttribute("finish", finish);
        request.setAttribute("category", categoryrefinement);
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}