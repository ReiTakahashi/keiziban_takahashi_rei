package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", shop_id");
			sql.append(", department_id");
			sql.append(", is_working");//後から付け足し
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); //password
			sql.append(", ?"); // name
			sql.append(", ?"); // shop_id
			sql.append(", ?"); // department_id
			sql.append(", ?"); // is_working
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getShop_id());
			ps.setInt(5, user.getDepartment_id());
			ps.setInt(6, user.getIs_working());


			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//login_idとpasswordをもとにユーザーを探してくる処理。
	public User getUser(Connection connection, String login_id,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> AllUser(Connection connection, int limitNum) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users";

			ps = connection.prepareStatement(sql);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public User getEditUser(Connection connection, int limitNum,int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users where users.id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int shop_id = rs.getInt("shop_id");
				int department_id = rs.getInt("department_id");
				int is_working = rs.getInt("is_working");


				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setName(name);
				user.setPassword(password);
				user.setShop_id(shop_id);
				user.setDepartment_id(department_id);
				user.setIs_working(is_working);

//user型で詰める
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public void update(Connection connection, User user) throws Exception {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();//順番通り
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			if(user.getPassword().isEmpty() == false) {
				sql.append(", password = ?");
			}
			sql.append(", shop_id = ?");
			sql.append(", department_id = ?");

			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            if(user.getPassword().isEmpty() ) {
            	user.setPassword(user.getPassword());
            	ps.setInt(3, user.getShop_id());
            	ps.setInt(4, user.getDepartment_id());
            	ps.setInt(5, user.getId());
            }else {
            	ps.setString(3, user.getPassword());
            	ps.setInt(4, user.getShop_id());
            	ps.setInt(5, user.getDepartment_id());
            	ps.setInt(6, user.getId());
            }

            System.out.println(ps);
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new Exception();
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

	}

	public void is_WorkingUpdate(Connection connection, User user) throws Exception {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_working= ?");

			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIs_working());
            ps.setInt(2, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new Exception();
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

	}
	public User duplicate(Connection connection, String login_id) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("select * from users where login_id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, login_id);

			//クエリ文を実行
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}