package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, String starttime , String finishtime,String categoryrefinement) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.text as text, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.subject as subject, ");
            sql.append("posts.category as category, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_data as created_data ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("where created_data BETWEEN  ? and  ? ");
            if(!(StringUtils.isEmpty(categoryrefinement))) {
            	 sql.append("and category LIKE ? ");
            }
            sql.append("ORDER BY created_data DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1,starttime);
			ps.setString(2, finishtime);
			if (!StringUtils.isEmpty(categoryrefinement)) {
				ps.setString(3,"%" + categoryrefinement + "%");
			}


			System.out.println(ps);

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	int user_id = rs.getInt("user_id");
                String name = rs.getString("name");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp created_data = rs.getTimestamp("created_data");


                UserMessage message = new UserMessage();
                message.setId(id);
                message.setUser_id(user_id);
                message.setName(name);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setcreated_date(created_data);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}