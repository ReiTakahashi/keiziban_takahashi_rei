package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 1000;

	public List<User> getUser() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao UserDao = new UserDao();
	        List<User> ret = UserDao.AllUser(connection, LIMIT_NUM);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public User getUser(int id) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao UserDao = new UserDao();
	        User ret = UserDao.getEditUser(connection, LIMIT_NUM, id);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	  public void update(User user) throws Exception {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            if(user.getPassword().isEmpty() == false) {
	            	String encPassword = CipherUtil.encrypt(user.getPassword());
	            	user.setPassword(encPassword);
	            }

	            UserDao userDao = new UserDao();
	            userDao.update(connection, user);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
	  public void is_working(User user) throws Exception {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            userDao.is_WorkingUpdate(connection, user);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}