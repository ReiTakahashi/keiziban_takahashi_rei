package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();


			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(String start, String finish,String categoryrefinement) {//servret宛て
		Connection connection = null;

		try {

			connection = getConnection();
			String starttime ="2019/01/01 00:00:00";
			Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
			String finishtime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(currentTimestamp);



			if (start != null && !(start.isEmpty())) {
				starttime = start+" 00:00:00";
			}
			if (finish != null && !(finish.isEmpty())) {
				finishtime = finish+" 23:59:59";
			}

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM,starttime,finishtime,categoryrefinement);//dao宛て

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void delete(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


}