<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>トップページ</title>
<link href="css/top.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">
	function disp() {

		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm('本当によろしいですか？')) {
			return true;
		} else {
			return false;
		}

		var startPos = 0,winScrollTop = 0;
		$(window).on('scroll',function(){
		    winScrollTop = $(this).scrollTop();
		    if (winScrollTop >= startPos) {
		        $('.header').addClass('hide');
		    } else {
		        $('.header').removeClass('hide');
		    }
		    startPos = winScrollTop;
		});
		$(window).on('scroll',function(){
		    winScrollTop = $(this).scrollTop();
		    if (winScrollTop >= startPos) {
		        if(winScrollTop >= 200){
		            $('.header').addClass('hide');
		        }
		    } else {
		        $('.header').removeClass('hide');
		    }
		    startPos = winScrollTop;
		});
	}
</script>
<body>

	<div class="main-contents">



		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ loginUser.department_id == 2 || loginUser.department_id == 3 || loginUser.department_id == 4 }">
				<a href="./">ホーム</a>
				<a href="logout">ログアウト</a>
				<a href="message">メッセージポスト</a>
			</c:if>
			<c:if test="${ loginUser.department_id == 1 }">
				<a href="./">ホーム</a>
				<a href="logout">ログアウト</a>
				<a href="message">メッセージポスト</a>
				<a href="management">管理</a>
			</c:if>
		</div>

<div class="main-content">


		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


		<c:if test="${ not empty loginUser }">


			<div class="profile">
					<h2>
						こんにちは
						<c:out value="${loginUser.name}" />
						さん
					</h2>
				</div>
		</c:if>

	<div class = "Serch">
		<form action="./" method="get">
			Start date:<input type="date" name="start"
				value="${start}"> <br> Finish date:<input
				type="date" name="finish" value="${finish}"><br>



		<br /> カテゴリ検索

			<input name="category" value="${category}"> <input
				type="submit" value="絞り込み" />
		</form>
		<br />
		</div>
<div class = "main">
		<c:if test="${ not empty loginUser }">
			<c:forEach items="${messages}" var="message">
			<div class = "message">
					<span class="name">名称 <c:out value="${message.name}" />さん
					</span>
					<br>
					カテゴリー
					<c:out value="${message.category}" />
					<br>
					件名
					<c:out value="${message.subject}" />
					<br>
					本文  <br>
					<c:forEach var="s" items="${fn:split(message.text, '
')}">
						<c:out value="${s}" />
						<br>
					</c:forEach>
					<c:out value="${message.created_date}" />
				<br />

				<c:if test="${loginUser.id == message.user_id }">
					<form action="messageDelete" method="post"onClick="return disp();">
						<input type="hidden" name="post_id" value="${message.id}">
						<input type="submit" value="削除" />
					</form>
				</c:if>
				<br />
				<label for="text"></label>
					</div>
				<br />
				<div class = "comment">
				<c:forEach items="${comments}" var="comment">
					<c:if test="${ not empty loginUser }">
						<c:if test="${comment.post_id == message.id}">
					コメント<br>
					<c:forEach var="s" items="${fn:split(comment.text, '
')}">
						<c:out value="${s}" />
						<br>
					</c:forEach>
										投稿者<br> <c:out value="${comment.name}" />
										さん
										<c:if test="${loginUser.id == comment.user_id }">
										<br>
											<form action="commentDelete" method="post"onClick="return disp();">
												<br /> <input type="hidden" name="id" value="${comment.id}">
												<input type="submit" value="削除" />
											</form>
										</c:if>
									<br />
						</c:if>
					</c:if>
				</c:forEach>
				</div>
				<form action="comment" method="post">
					<textarea name="text" id="text" cols="50" rows="4"
						class="tweet-box" /></textarea>
					<input type="hidden" name="post_id" value="${message.id}">
					<input type="submit" value="投稿" />
				</form>
			</c:forEach>
		</c:if>
		</div>
		</div>

	</div>
</body>
</html>