<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP&display=swap" rel="stylesheet">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link href="css/top.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="login_id">ログインID</label> <input name="login_id"
				id="login_id" value="${user.login_id}"/><br /> <label
				for="name">名称</label> <input name="name" id="name" value="${user.name}"/> <br /> <label
				for="password">パスワード</label> <input name="password" type="password"
				id="password" /> <br />
			<label for="password2">確認</label> <input name="password2"
				type="password" /> <br /> 支店<select name="shop">
				<c:forEach items="${shops}" var="shop">
				<c:choose>
					<c:when test = "${shop.id == user.shop_id}">
					<option  value="${shop.id}"selected >${shop.name}</option> </c:when>
					<c:when test = "${shop.id != user.shop_id}">
					<option  value="${shop.id}" >${shop.name}</option> </c:when>
				</c:choose>
				</c:forEach>
			</select> <br /> 役職<select name="department">
				<c:forEach items="${departments}" var="department">
				<c:choose>
					<c:when test = "${department.id == user.department_id}">
					<option  value="${department.id}"selected >${department.name}</option> </c:when>
					<c:when test = "${department.id != user.department_id}">
					<option value="${department.id}">${department.name}</option> </c:when>
				</c:choose>

				</c:forEach>
			</select> <input type="submit" value="登録" /> <br /> <a href="management">戻る</a>
		</form>


		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>