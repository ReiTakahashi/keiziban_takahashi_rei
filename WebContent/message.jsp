
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP&display=swap" rel="stylesheet">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿フォーム</title>
<link href="css/top.css" rel="stylesheet" type="text/css">
</head>
<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<c:if test="${ not empty loginUser }">
		<div class="profile">
			投稿画面

			<div class="main-contents">
				<form action="message" method="post">
					<br /> <label for="subject">件名</label> <input name="subject"
						id="subject" value = "${message.subject}"/> <br /> <label for="category">カテゴリー</label> <input
						name="category" id="category" value = "${message.category}"/> <br /> <label for="text"
						>本文</label>
					<textarea name="text" id="text" cols="100"  rows="5"
						class="messageform" /> ${message.text} </textarea>
					<br />
					<input type="submit" value="投稿"> <br /> <a href="./">戻る</a>
				</form>
				<div class="copyright">Copyright(c)Your Name</div>
			</div>
		</div>

	</c:if>



</body>
</html>